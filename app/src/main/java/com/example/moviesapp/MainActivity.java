package com.example.moviesapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import android.os.Bundle;


public class MainActivity extends AppCompatActivity {

    FragmentManager manager = this.getSupportFragmentManager();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        openFirstScreen();
    }

public void openFirstScreen(){
        manager.beginTransaction()
                .hide(manager.findFragmentById(R.id.second_screen))
                .show(manager.findFragmentById(R.id.first_screen))
                .commit();
    }
}