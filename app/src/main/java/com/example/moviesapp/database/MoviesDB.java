package com.example.moviesapp.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.moviesapp.Movie;

import java.util.ArrayList;

public class MoviesDB {
    public static final String KEY_ID = "_id";
    public static final String KEY_SEARCH = "_search";
    private final String DATABASE_NAME = "MoviesDB";
    private final String DATABASE_TABLE = "search";
    private final int DATABASE_VERSION = 1;
    private DBHelper dbhelper;
    private final Context dbContext;
    private SQLiteDatabase database;
    public MoviesDB (Context context) {
        dbContext = context;
    }

    private class DBHelper extends SQLiteOpenHelper {
        public DBHelper (Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            String sqlQuery = "CREATE TABLE " + DATABASE_TABLE + " (" + KEY_ID +
                    " INTEGER PRIMARY KEY AUTOINCREMENT, " + KEY_SEARCH +
                    " TEXT NOT NULL);";
            db.execSQL(sqlQuery);
        }
        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE);
            onCreate(db);
        }
    }

    public MoviesDB open() throws SQLException {
        dbhelper = new DBHelper(dbContext);
        database = dbhelper.getWritableDatabase();
        return this;
    }
    public void close() {
        dbhelper.close();
    }

    public long createEntry(String s) {
        ContentValues content = new ContentValues();
        content.put(KEY_SEARCH, s);
        return database.insert(DATABASE_TABLE, null, content);
    }

    public ArrayList<String> getData() {
        String[] col = new String[] {KEY_ID, KEY_SEARCH};
        Cursor cursor = database.query(DATABASE_TABLE, col, null, null, null, null, null);
        int searchInd = cursor.getColumnIndex(KEY_SEARCH);

        ArrayList<String> list = new ArrayList<>();

        cursor.moveToLast();
        for(; !cursor.isBeforeFirst(); cursor.moveToPrevious()) {
            list.add(cursor.getString(searchInd));
        }
        cursor.close();
        return list;
    }

    public void deleteEntry(){
        String[] col = new String[] {KEY_ID, KEY_SEARCH};
        Cursor cursor = database.query(DATABASE_TABLE, col, null, null, null, null, null);
        int idInd = cursor.getColumnIndex(KEY_ID);
        cursor.moveToFirst();
        int id = cursor.getInt(idInd);
        cursor.close();
        String query = "DELETE FROM " + DATABASE_TABLE + " WHERE " + KEY_ID + " = " + id + ";";
        database.execSQL(query);
    }

}