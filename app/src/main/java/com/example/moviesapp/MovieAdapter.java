package com.example.moviesapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.ViewHolder> {

    private static List<Movie> movies;

    public MovieAdapter(Context context, List<Movie> list)
    {
        movies = list;
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        TextView tvTitle, tvDescription, tvDate, tvVote;
        ImageView ivPoster;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvTitle = itemView.findViewById(R.id.tvTitle);
            tvDescription = itemView.findViewById(R.id.tvDescription);
            tvDate = itemView.findViewById(R.id.tvDate);
            tvVote = itemView.findViewById(R.id.tvVote);
            ivPoster = itemView.findViewById(R.id.ivPoster);

        }
    }

    @NonNull
    @Override
    public MovieAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MovieAdapter.ViewHolder holder, int position) {

        holder.itemView.setTag(movies.get(position));

        holder.tvTitle.setText(movies.get(position).getTitle());
        holder.tvDescription.setText(movies.get(position).getOverview());
        holder.tvDate.setText(movies.get(position).getRelease_date());
        holder.tvVote.setText(movies.get(position).getVote_average().toString());
        Picasso picasso = Picasso.get();
        picasso.load("https://image.tmdb.org/t/p/w92" + movies.get(position).getPoster_path()).into(holder.ivPoster);
    }

    @Override
    public int getItemCount() {
        return movies.size();
    }
}
