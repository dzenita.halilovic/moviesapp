package com.example.moviesapp;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface MoviesEndPoints {

    @GET("/3/search/movie")
    Call<MoviesResponse> getMovies(
            @Query("api_key") String api_key,
            @Query("query") String query,
            @Query("page") String page
     );
}
