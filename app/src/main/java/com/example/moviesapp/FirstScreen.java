package com.example.moviesapp;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.moviesapp.database.MoviesDB;

import java.util.ArrayList;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class FirstScreen extends Fragment {

    FragmentManager manager;
    View view;
    String searchedMovie = "";
    Integer page = 1;
    Integer totalPages = 0;
    MoviesDB db;
    ListView listMovies;
    ArrayAdapter<String> adapter;


    public FirstScreen() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_first_screen, container, false);
        db = new MoviesDB(getActivity());
        manager = this.getActivity().getSupportFragmentManager();

        listMovies = view.findViewById(R.id.list);
        adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, getSuggestions());
        listMovies.setAdapter(adapter);

        listMovies.setVisibility(View.GONE);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setupSearchView();

        listMovies.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                api(adapter.getItem(i));
            }
        });

        SecondScreen secondScreen = (SecondScreen) manager.findFragmentById(R.id.second_screen);
        secondScreen.getRecyclerView().addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(!recyclerView.canScrollVertically(1)){
                    if(page < totalPages){
                        Toast.makeText(getContext(),"Loading...", Toast.LENGTH_SHORT).show();
                        page += 1;
                        api(searchedMovie);
                    }
                }
            }
        });
    }


    public void setupSearchView(){
        final SearchView searchV = view.findViewById(R.id.searchView);

        searchV.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if(!b)
                    listMovies.setVisibility(View.GONE);
                else
                    listMovies.setVisibility(View.VISIBLE);
            }
        });


        searchV.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                page = 1;
                searchedMovie = query;
                api(searchedMovie);

                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                adapter.getFilter().filter(s);
                return false;
            }
        });

    }

    public void alertView(){
        AlertDialog alertDialog = new AlertDialog.Builder(getContext()).create();
        alertDialog.setTitle("Search failed!");
        alertDialog.setMessage("No movies were found.");
        alertDialog.setIcon(R.drawable.alert);
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    private ArrayList<String> getSuggestions() {
        db.open();
        ArrayList<String> suggestions = db.getData();
        db.close();
        return suggestions;
    }

    private void deleteOldestSuggestion() {
        db.open();
        db.deleteEntry();
        db.close();
    }

    private void createNewSuggestion(String query) {
        db.open();
        db.createEntry(query);
        db.close();
    }

    private void hideSuggestions(){
        SearchView searchV = view.findViewById(R.id.searchView);
        searchV.onActionViewCollapsed();
    }

    public void api(final String query){
        OkHttpClient client = new OkHttpClient();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api.themoviedb.org/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        MoviesEndPoints request = retrofit.create(MoviesEndPoints.class);
        Call<MoviesResponse> call = request.getMovies(getString(R.string.API_KEY), query, page.toString());

        call.enqueue(new Callback<MoviesResponse>() {
            @Override
            public void onResponse(Call<MoviesResponse> call, Response<MoviesResponse> response) {
                if (response.body() == null) {
                    return;
                }

                else if(response.body().getResults().size() == 0 && page == 1){
                    alertView();
                    return;
                }

                else{
                    adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, getSuggestions());
                    listMovies.setAdapter(adapter);
                    totalPages = response.body().getTotalPages();
                    if(!getSuggestions().contains(query)){
                        if(getSuggestions().size() >= 10)
                            deleteOldestSuggestion();
                        createNewSuggestion(query);
                    }
                    openSecondScreen();
                    hideSuggestions();

                    final SecondScreen secondScreen = (SecondScreen) manager.findFragmentById(R.id.second_screen);
                        secondScreen.setRecyclerViewAdapter((ArrayList<Movie>)  response.body().getResults());
                }
            }


            @Override
            public void onFailure(Call<MoviesResponse> call, Throwable t) {
                Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    public void openSecondScreen(){
        manager.beginTransaction()
                .show(manager.findFragmentById(R.id.second_screen))
                .hide(manager.findFragmentById(R.id.first_screen))
                .addToBackStack(null)
                .commit();
    }
}
